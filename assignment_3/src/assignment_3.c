#include <stdio.h>
#include <stdlib.h>



int isEmpty(int *arr)
{
	int check = 1;
	for (int i = 0; i < 10; i++)
	{
		if (arr[i] != 0)
			check = 0;
	}
	return check;
}

int avaIndex(int *arr)
{
	int check=0;
	for(int i=0;i<10;i++)
	{
		if(arr[i]==0){
			if(check==0)
				printf("Available Index : ");
			printf("%d ",i);
			check = 1;
		}
	}
	return check;
}

int avaIndex_Val(int *arr)
{
	int check = 0;
	for (int i = 0; i < 10; i++)
	{
		if (arr[i] != 0) {
			if(check==0)
				printf("\nIndex Number");
			printf("\n %2d %5d", i, arr[i]);
			check = 1;
		}
	}
	return check;
}


void add_Number(int *arr)
{
	int index,num;
		if (avaIndex(arr))
		{
			printf("\nEnter Index Number : ");
			scanf("%d", &index);
			printf("\nEnter a Number to add : ");
			scanf("%d", &num);
			arr[index] = num;
			printf("Number is successfully Added\n");
		}
		else
			printf("Array is Full");
}

void delete_Number(int *arr)
{
	int index;
		if (avaIndex_Val(arr))
		{
			printf("\nEnter Index Number to delete : ");
			scanf("%d", &index);
			arr[index]=0;
			printf("Number is Successfully deleted");
		} else
			printf("Array is Empty");
	}


void Max_Number(int *arr)
{
	int max = 0,index;
		if(isEmpty(arr))
		{
			printf("Array is Empty");
		} else
		{
			for (int i = 0; i < 10; i++)
			{
				if (arr[i] > max){
					max = arr[i];
					index =i;
				}
			}
			printf("\nMax number is : %d and Index : %d", max,index);
		}
}

void Min_Number(int *arr)
{
	int min = 10000000,index;
		if (isEmpty(arr))
		{
			printf("Array is Empty");
		} else
		{
			for (int i = 0; i < 10; i++)
			{
				if (arr[i] < min && arr[i]!=0)
				{
					min = arr[i];
					index = i;
				}
			}
			printf("\nMin number is : %d and Index : %d", min,index);
		}
}

void sumOfNumbers(int *arr)
{
	int sum = 0;
		if (isEmpty(arr))
		{
			printf("\nArray is empty");
		} else
		{
			for (int i = 0; i < 10; i++)
			{
				sum += arr[i];
			}
			printf("\nSum = %d", sum);
		}
}

void view_Array(int *arr)
{
	for (int i = 0; i < 10 ; i++)
	{
			printf("%d ", arr[i]);
	}
}


int menu_list(void)
	{
		int choice;
		setvbuf(stdout,NULL,_IONBF,0);
		printf("\nChoose operation to perform");
		printf("\n0. Exit");
		printf("\n1. Add Number");
		printf("\n2. Delete Number");
		printf("\n3. Maximum Number");
		printf("\n4. Minimum Number");
		printf("\n5. Sum of Numbers");
		printf("\n6. View Array");
		printf("\nEnter Choice");
		scanf("%d",&choice);
		return choice;
	}

int main(void)
	{
		int choice,arr[10];
		for(int i=0;i<10;i++)
			arr[i]=0;

		while((choice=menu_list())!=0)
		{
		switch(choice)
		{
		case 1:
			add_Number(arr);
			break;
		case 2:
			delete_Number(arr);
			break;
		case 3:
			Max_Number(arr);
			break;
		case 4:
			Min_Number(arr);
			break;
		case 5:
			sumOfNumbers(arr);
			break;
		case 6:
			view_Array(arr);
			break;
		}
		}
		return EXIT_SUCCESS;
	}
