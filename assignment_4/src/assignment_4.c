#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Book
{
	int id, price;
	char name[20];
} BOOK;

void mergeSort(BOOK book[],int,int);
void merge(BOOK book[],int,int,int);
void printArray(struct Book[],int);

int cmpfunc(const void * book1, const void * book2)
{
	int l = *((BOOK *)book1)->name;
	int r = *((BOOK *)book2)->name;
	return (l - r);
}

int main(void)
{
	BOOK book[10];

// Storing Data into the structure array
	char name[10][20] = { "Prince", "Pride and Justice", "Scary night", "Inland", "Invisible man",
			"Beloved", "War and Peace", "In search of lost time", "Little Women",
			"The Water Cure"};
	int id[10] = { 15, 20, 120, 156, 250, 56, 89, 895, 54, 78};
	int price[10] = { 569, 896, 830, 784, 489, 820, 356, 211, 569, 458};

	int len = sizeof(book) / sizeof(book[0]);

	for (int i = 0; i < len; i++)
	{
		book[i].id = id[i];
		strcpy(book[i].name, name[i]);
		book[i].price = price[i];
	}

// Original Array without sort
	printf("\n**** Original Book list Before Sorting : ****\n");
	printArray(book, len);

// Merge Sort According to Price
	mergeSort(book, 0, len - 1);
	printf("\n**** Array After Merge Sort According to Price : ****\n");
	printArray(book, len);

// qsort According to Name
	qsort(book,len, sizeof(BOOK),cmpfunc);
	printf("\n**** Array After qSort According to Name : ****\n");
	printArray(book, len);

	return EXIT_SUCCESS;
}

void mergeSort(BOOK book[], int p, int r)
{
	int q;
	if (p < r)
	{
		q = (p + r) / 2;
		mergeSort(book, p, q);
		mergeSort(book, q + 1, r);
		merge(book, p, q, r);
	}
}

// function to merge the subarrays
void merge(BOOK book[], int p, int q, int r)
	{
	BOOK temp[r+1];   //same size of book[]
	int i, j, k;
	k = 0;
	i = p;
	j = q + 1;
	while (i <= q && j <= r)
	{
		if (book[i].price < book[j].price)
		{
			temp[k++] = book[i++];
		} else {
			temp[k++] = book[j++];
		}
	}

	while (i <= q)
	{
		temp[k++] = book[i++];
	}

	while (j <= r)
	{
		temp[k++] = book[j++];
	}

	for (i = r; i >= p; i--)
	{
		book[i] = temp[--k];  // copying back the sorted list to a[]
	}
}

// function to print the array
void printArray(BOOK book[], int len)
{
	printf("\n Id \t Name \t\t Price\n");
	for (int i = 0; i < len; i++)
	{
		printf("%4d\t%s %8d\n", book[i].id, book[i].name, book[i].price);
	}
}
